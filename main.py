from locust import User, HttpUser, task, HttpLocust, TaskSet
import os


class OtteraTest(HttpUser):

    host = "https://stage.ottera.tv"
    
    @task
    def first_test(self):
        self.client.get("/")

    @task
    def second_test(self):
        self.client.get("/user/login")

        