FROM python:3.8

RUN pip install locust

WORKDIR /home

COPY . .

ENTRYPOINT ["/bin/bash", "-c"]

CMD ["locust -f main.py"]